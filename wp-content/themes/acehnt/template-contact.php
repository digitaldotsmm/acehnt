<?php /* Template Name: Contact us */ ?>
<?php get_header() ?>


<div id="category section-breadcrumb">
	<div id="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
		<div class="container text-center">
			<h1 class="bannerheader pagetitle text-uppercase fontlora"><?php the_title() ?></h1>
		</div>
	</div>
</div>

	
<div class="container">
	<div class="row margintop50 marginbottom50">
		<div class="col-xs-12 col-sm-12 col-lg-8 col-md-8">
			<h2 class="fontlora underline mbtitle">CONTACT FORM</h2>
			<div id="thank_message" style="display: none;" class="thankumsg">
                <p>"Thank you for choosing to stay with us. we shall reply to you at the soonest."</p>
            </div> 
            
			<form id="reservation-tour" action="<?php the_permalink(); ?>" class="form-horizontal" method="POST"> 
			<div class="formwrapper">
				<div class="form-group">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 text-center">
							<span class="input input--yoshiko">
								<input class="input__field input__field--yoshiko required" type="text" id="name" name="name" />
								<label class="input__label input__label--yoshiko" for="name">
									<span class="input__label-content input__label-content--yoshiko" data-content="NAME">Your Name</span>
								</label>
							</span>		
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 text-center">
							<span class="input input--yoshiko">
								<input class="input__field input__field--yoshiko required" type="email" id="email" name="email" />
								<label class="input__label input__label--yoshiko" for="email">
									<span class="input__label-content input__label-content--yoshiko" data-content="EMAIL">Your Email</span>
								</label>
							</span>		
						</div>
					</div>
				</div>
			  	<div class="form-group">
			  		<div class="row">
			  			<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
				  			<span class="input input--yoshiko">
								<input class="input__field input__field--yoshiko required" type="text" id="company" name="company"/>
								<label class="input__label input__label--yoshiko" for="company">
									<span class="input__label-content input__label-content--yoshiko" data-content="COMPANY NAME">Your Company</span>
								</label>
							</span>		
			  			</div>
			  			<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
			  				<span class="input input--yoshiko">
								<input class="input__field input__field--yoshiko required" type="text" id="phone" name="phone"/>
								<label class="input__label input__label--yoshiko" for="phone">
									<span class="input__label-content input__label-content--yoshiko" data-content="PHONE">Your Phone</span>
								</label>
							</span>		
			  			</div>
			  		</div>
			  	</div>
			  	<div class="form-group">
				  	<span class="input input--yoshiko">
						<textarea class="input__field input__field--yoshiko required" id="message" cols="40" rows="10" name="message"></textarea>
						<label class="input__label input__label--yoshiko" for="message">
							<span class="input__label-content input__label-content--yoshiko" data-content="MESSAGE">YOUR MESSAGE</span>
						</label>
					</span>
			  	</div>
			  	<div class="form-group"> 
					<input type="hidden" name="form_type" value="customize-tour">
                    <input type="submit" class="" value="SEND" id="sendbtn">
                    <div id="processing" style="display: none"><p class="load_process">Processing...please wait</p></div>
			  	</div>
			  	</div>
			</form>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-4 col-md-4">
			<h2 class="fontlora underline mbtitle">CONTACT ADDRESS</h2>
				<ul class="list-icons list-unstyled">
					<?php if($THEME_OPTIONS['info_address']): ?>
						<li><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_address']; ?></li>
					<?php endif; ?>
                	<?php if($THEME_OPTIONS['info_hotline']): ?>
						<li><i class="fa fa-mobile" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_hotline']; ?></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['info_email']): ?>
						<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?>"><?php echo $THEME_OPTIONS['info_email']; ?></a></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['info_website']): ?>
						<li><i class="fa fa-globe" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_website']; ?></li>
					<?php endif; ?>
				</ul>
				<hr>
				<div class="clearfix">
				<h3 class="greencolor fontlora">Follow Us</h3>
				<?php if($THEME_OPTIONS['facebookid']): ?>
				<a href="<?php echo $THEME_OPTIONS['facebookid'] ?>" class="facebookcontact" target="__blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<?php endif; ?>
				</div>

		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 margintop20">
			<h2 class="fontlora text-center contacttitle"><span class="dot"></span>OFFICE LOCATION<span class="dot1"></span></h2>
			<div class="margintop20 marginbottom50">
				<div id="googleMap" style="width:100%;height:400px;"></div>
			</div>
		</div>
	</div>
</div>

<script>
			(function() {
				
				if (!String.prototype.trim) {
					(function() {
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}

				[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );

				[].slice.call( document.querySelectorAll( 'textarea.input__field' ) ).forEach( function( inputEl ) {
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );


				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}

				function onInputBlur( ev ) {
					if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}
			})();

	$(document).ready(function(){

		var mycenter = new google.maps.LatLng(16.785029,96.155150);
		function initMap() {
			map = new google.maps.Map(document.getElementById('googleMap'), {
				center: mycenter,
				zoom: 17,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			});

			marker=new google.maps.Marker({
	            position: mycenter,
				map: map,
				animation: google.maps.Animation.DROP
			});
			
			var contentString = '<div id="infowindow">'+' <div class="text-center marginbottom20"><img src="<?php echo $THEME_OPTIONS['logo'] ?>" alt="Comet Design & Decoration Logo" /></div>'+'<div id="bodyinfowindow">'+
			'<?php if($THEME_OPTIONS['info_address']): ?><p><i class="fa fa-map-marker"></i> : <?php echo $THEME_OPTIONS['info_address'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_hotline']): ?><p><i class="fa fa-phone"></i> : <?php echo $THEME_OPTIONS['info_hotline'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_email']): ?><p><i class="fa fa-envelope"></i> : <?php echo $THEME_OPTIONS['info_email'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_website']): ?><p><i class="fa fa-globe"></i> : <?php echo $THEME_OPTIONS['info_website'];?></p><?php endif; ?>'+
			'</div>'+'</div>';

			var infowindow = new google.maps.InfoWindow({
          		content: contentString
        	});

			marker.setMap(map);
			marker.addListener('click', function() {
          		infowindow.open(map, marker);
        	});
			
		}

    	google.maps.event.addDomListener(window, 'load', initMap);
    });
</script>
<?php get_footer() ?>