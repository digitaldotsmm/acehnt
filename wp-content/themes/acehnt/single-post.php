<?php get_header() ?>

<!-- <div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
			<h1 class="pagetitle text-uppercase">News</h1>
		</div>
	</div>
</div> -->
<div id="banner">
	<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	<div class="container text-center">
		<h1 class="bannerheader pagetitle text-uppercase fontlora">News</h1>
	</div>
</div>
<div class="sgnewwrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<?php 
					$galleries = get_field('gallery',$post->ID);
					$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                    $post_image = aq_resize($img_url[0], 500, 300, true, true, true);
				?>
				<div class="news-header">
                    <h2 class="fontlora greencolor text-uppercase"><?php the_title(); ?></h2>
                    <span class="news-date"><?php the_time('M j, Y'); ?></span>
                </div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-6  col-md-6 col-md-offset-3">
						<div class="newswrap marginbottom20">
							<a href="<?php echo $post_image; ?>" rel="prettyPhoto" title="<?php the_title(); ?>">
								<img src="<?php echo $post_image; ?>" class="img-responsive img-thumbnail">
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
	                    <div class="news-intro">
	                        <p><?php echo apply_filters("the_content", $post->post_content); ?></p>
	                    </div>
					</div>
				</div>
				
				<div class="row">
				<?php 
					
					if ($galleries) :
					foreach ($galleries as $gallery): 
						$gallery_img = aq_resize($gallery['url'],300,200,true,true,true); 
				?>
					<div class="col-xs-12 col-sm-6 col-lg-3 col-md-3 centergallery">
						<div class="marginbottom10 shadowimg">
							<a href="<?php echo $gallery['url'];?>" rel="prettyPhoto[<?php echo $gallery;?>]" title="<?php echo $post->post_title; ?>">
								<img src="<?php echo $gallery_img; ?>" class="img-responsive">
							</a>
						</div>
					</div>
				<?php endforeach ?>
				<?php endif ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>