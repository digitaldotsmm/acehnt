<?php get_header(); ?>
<!-- <div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
			<h1 class="pagetitle text-uppercase"><?php the_title() ?></h1>
		</div>
	</div>
</div> -->
<div id="banner">
	<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	<div class="container text-center">
		<h1 class="bannerheader pagetitle text-uppercase fontlora"><?php the_title() ?></h1>
	</div>
</div>

	<div class="container">
		<div class="row  margintop50 marginbottom50">
			<div class="col-md-8">
				<div class="pagecontent">
					<?php echo apply_filters("the_content",$post->post_content); ?>
				</div>
			</div>
			<div class="col-md-4">
				<?php $img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
				<div class="aboutimg text-center">
					<img src="<?php echo $img_url[0]; ?>">
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
