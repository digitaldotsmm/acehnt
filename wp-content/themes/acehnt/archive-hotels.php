<?php get_header() ?>

<div id="banner">
	<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	<div class="container text-center">
		<h1 class="bannerheader pagetitle text-uppercase fontlora">Our Hotels</h1>
	</div>
</div>

<div id="ourhotels">
	<div class="container">
		<div class="ourhotelswrapper">
		<?php
			    $args = array(
			        'post_type' => ACE_TYPE_HOTEL,
			        'posts_per_page' => -1,
			        'post_status' => 'publish',
			        );
			    $hotels = get_posts($args);

			    $styeltwo = [$hotels[0],$hotels[1]];

			    $other_hotels = array_slice($hotels,2);
			?>

			<div class="row">
				<?php foreach ($styeltwo as $two): ?>
				<?php 
					$hotel_link = get_permalink($two->ID);
				    $galleries = get_field('gallery',$two->ID);
				    $rooms = get_field('room',$two->ID);
				    $faciliteis = get_field('front_facilities',$two->ID);
				    $hoteladdress = get_field('hotel_address',$two->ID); 
				    $hotel_phone = get_field('hotel_phone',$others->ID); 
			    $hotel_email = get_field('hotel_email',$others->ID); 
					$hotellocation = get_field('location',$two->ID); 
					$hotellat = $hotellocation[0]['lat'];
					$hotellang = $hotellocation[0]['lang'];

					$hotelimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($two->ID), 'full'); 
		        	$post_image = aq_resize($hotelimg_url[0], 560, 310, true, true, true);
				?>
					
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 marginbottom10">
					<div class="event-classic-item">
						<div class="inner-post">
							<a href="#" title="<?php echo $two->post_title; ?>">
								<img src="<?php echo $post_image; ?>" alt="<?php echo $two->post_title; ?>"></a>                            
								<div class="light-box">
								</div>
							<div class="info-classic">
								<div class="title">
									<a href="<?php echo $hotel_link; ?>"><?php echo $two->post_title; ?></a>
								</div>
								
								<div class="classic-read-more">
									<p class="address">
										<?php echo $hoteladdress; ?>
									</p>
									<a class="btnhotel" href="<?php echo $hotel_link; ?>" title="King Suite Room">Detail</a>
									<a class="office_map_link btnhotel" data-title="<?php echo $two->post_title; ?>"  href="#office_map" rel="<?php echo $hotellat; ?>,<?php echo $hotellang; ?>">See Map</a>
									<span data-title="<?php echo $hotel_phone; ?>"></span>
									<span data-title="<?php echo $hotel_email; ?>"></span>
								</div>
							</div>
						</div>
					</div>
			    </div>
				<?php endforeach ?>
			    
		    </div>

		    <?php $count =1;foreach ($other_hotels as $others): ?>
		    	
	    	<?php 
		    	$hotel_link = get_permalink($others->ID);
			    $galleries = get_field('gallery',$others->ID);
			    $rooms = get_field('room',$others->ID);
			    $faciliteis = get_field('front_facilities',$others->ID);
			    $hoteladdress = get_field('hotel_address',$others->ID); 
			    $hotel_phone = get_field('hotel_phone',$others->ID); 
			    $hotel_email = get_field('hotel_email',$others->ID); 
				$hotellocation = get_field('location',$others->ID); 
				$hotellat = $hotellocation[0]['lat'];
				$hotellang = $hotellocation[0]['lang'];
				
	        	$hotelimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($others->ID), 'full'); 
	        	$post_image = aq_resize($hotelimg_url[0], 260, 240, true, true, true);
	        	if ($count == 1 || $count%4 == 1) { echo "<div class='row'>"; } 
	    	?>


	    		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 " id="ht320">
	            	<div class="single_room_wrapper animatedParent clearfix">
		                <figure class="uk-overlay uk-overlay-hover">
		                    <div class="room_media">
		                        <a href="#"><img src="<?php echo $post_image; ?>" alt="<?php echo $others->post_title; ?>"></a>
		                    </div>
		                    <div class="room_title border-bottom-whitesmoke clearfix">
		                        <div class="left_room_title floatleft">
		                            <h6><?php echo $others->post_title; ?></h6>
		                        </div>
		                    </div>
		                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
		                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
		                            <a href="<?php echo $hotel_link; ?>"> <h5><?php echo $others->post_title; ?></h5></a>
		                            <div class="single_room_cost clearfix">
		                                <div class="floatleft">
							                <p><?php echo $hoteladdress; ?></p>
		                                    <a class="btnhotel" href="<?php echo $hotel_link; ?>" >Detail</a>
		                                    <a class="office_map_link btnhotel" data-title="<?php echo $others->post_title; ?>" href="#office_map" rel="<?php echo $hotellat; ?>,<?php echo $hotellang; ?>">See Map</a>
							                <span data-title="<?php echo $hotel_phone; ?>"></span>
							                <span data-title="<?php echo $hotel_email; ?>"></span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </figure>
	            	</div>
	        	</div>
	        	<?php if ($count%4 == 0 || $tour_count == $count) { echo "</div>"; } ?>
		    <?php $count++;endforeach ?>
							
	        <div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-3">
					<h2 class="fontlora text-center"><span class="dot"></span>HOTEL LOCATION<span class="dot1"></span></h2>
					<div class="margintop20 marginbottom50">
						<div id="office_map"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	
<script type="text/javascript">
$(document).ready(function(){
  $('a[href^="#"]').on('click',function (e) {
      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
          'scrollTop': $target.offset().top
      }, 1200, 'swing', function () {
          window.location.hash = target;
      });
  });
});

    function getLatLng(str) {
        var arr = str.split(',');
        var lat = jQuery.trim(arr[0]);
        var lng = jQuery.trim(arr[1]);
        return new Array(lat,lng);
    }

    function office_map_initialize() {  
        var officeLatLngStr = jQuery('a.office_map_link:eq(0)').attr('rel');    
        var officeLatLngArr = getLatLng(officeLatLngStr);   
        var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
        var map = new google.maps.Map(document.getElementById("office_map"), {
            zoom: 6,
            center: officeLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        jQuery('a.office_map_link').each(function() {
            var office = jQuery(this);
            var officeLatLngStr = office.attr('rel');
            var officeLatLngArr = getLatLng(officeLatLngStr);
            var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
            var marker = new google.maps.Marker({
                clickable: true,
                flat: true,
                position: officeLatLng,
                map: map
            });
			
            var contentString = '<h4 class="greencolor fontlora">'+ $(this).attr('data-title') + '</h4><i class="greencolor fa fa-location-arrow" style="margin-right:6px;" aria-hidden="true"></i>' + $(this).parent().find('p').html() + '<br><i class="fa fa-envelope-o greencolor" style="margin-right:6px;" aria-hidden="true"></i>' + $(this).next('span').attr('data-title') + '<br><i class="fa fa-phone greencolor" style="margin-right:6px;" aria-hidden="true"></i>' + $(this).next('span').next('span').attr('data-title');;

            var infowindow = new google.maps.InfoWindow({
		      content: contentString
		    });

            marker.addListener('click', function() {
	          infowindow.open(map, marker);
	        });

            office.bind('click', function(e) {
                var office = jQuery(this);
                var officeLatLngStr = office.attr('rel');
                var officeLatLngArr = getLatLng(officeLatLngStr);
                var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
                map.panTo(officeLatLng);
                 var current_marker = new google.maps.Marker({
	                clickable: true,
	                flat: true,
	                position: officeLatLng,
	                map: map
	            });                
                infowindow.open(map, current_marker);
            });
        }); 
        
    }
    office_map_initialize();
    
    </script>

<?php get_footer() ?>