<?php get_header() ?>

<div id="banner">
	<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	<div class="container text-center">
		<h1 id="about" class="bannerheader pagetitle text-uppercase fontlora"><?php the_title() ?></h1>
	</div>
</div>
	    
<?php $hoteldata = get_fields($post->ID); ?>
<section class="hotelnavigation ">
	<div class="container animatedParent">
		<ul id="hoteltab">
			<li class="animated fadeInUpShort slow"><a href="#about" class=""><i class="fa fa-home marginright10" aria-hidden="true"></i><span>About</span></a></li>
			<li class="animated fadeInUpShort slow"><a href="#roomtype" class=""><i class="fa fa-bed marginright10" aria-hidden="true"></i><span>Room Type</span></a></li>
			<li class="animated fadeInUpShort slow"><a href="#hotelgallery" class=""><i class="fa fa-picture-o marginright10" aria-hidden="true"></i><span>Gallery</span></a></li>
			<li class="animated fadeInUpShort slow"><a href="#location" class=""><i class="fa fa-map-signs marginright10" aria-hidden="true"></i><span>Location</span></a></li>
		</ul>
	</div>	
</section>
<div id="sghotel">
	<div class="container animatedParent">
		<div class="row">
			<div id="aboutus">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
					<div class="text-center bigeffect">
						<?php $img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
						<?php if ($img_url): ?>
							<?php $post_image = aq_resize($img_url[0], 600, 400, true, true, true); ?>
							<img src="<?php echo $post_image; ?>" class="img-responsive animated fadeInLeft">
						<?php endif ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 animated fadeInRight delay-250">
				<?php if ($post->post_content): ?>
						<?php echo apply_filters("the_content", $post->post_content); ?>
				<?php endif ?>

				</div>
				<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 margintop30 marginbottom30 animated fadeInUp delay-500">
				
					<h3 class="fontlora">FACILITIES</h3>
					<?php if ($hoteldata['facilities']): ?>
                        <div class="addwrap">
                            <!-- <h3 class="fontlora">Facilities</h3> -->
                            <ul class="facilist">
                                <?php foreach ($hoteldata['facilities'] as $facilitie) { ?>
                                    <li><?php echo $facilitie['icon']; echo $facilitie['service'];?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php endif ?>
				</div>
			</div>
		</div>
		<!-- <hr> -->
		<div class="row marginbottom30">
			<?php if ($hoteldata['room']): ?>
			<div id="room">
				<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 animated fadeInDownShort delay-1750">
					<h4 id="roomtype" class="fontlora text-center f30 marginbottom30"><span class="dot"></span>ROOM TYPES<span class="dot1"></span></h4>
				</div>
				<?php foreach ($hoteldata['room'] as $key) { 
					$image = aq_resize($key['room_image']['url'], 458, 293, true, true, true); ?>
				<div class="clearfix marginbottom10">

					<div class="col-xs-12 col-sm-12 col-lg-5 col-md-5">
						<div class="borderimg effect2 animated fadeInDownShort delay-1750">
							<img src="<?php echo $image; ?>" alt="<?php echo $key['room_title']; ?>">
						</div>
					</div>		
					<div class="col-xs-12 col-sm-12 col-lg-7 col-md-7 fadeInDownShort animated delay-2000">
						<h3 class="fontlora"><?php echo $key['room_title']; ?></h3>
						<div><?php echo $key['room_data']; ?></div>
					</div>
				</div>
				<?php } ?> 
			</div>
			<?php endif ?>
		</div>

		<!-- <hr> -->

		<div class="row">
			<?php if ($hoteldata['gallery']): ?>
				<div id="hotelgallery" class="clearfix marginbottom20">
					<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 animated fadeInUp delay-2500">
						<h4 class="fontlora text-center f30 marginbottom30"><span class="dot"></span>GALLERY<span class="dot1"></span></h4>
					</div>
					<?php foreach ($hoteldata['gallery'] as $gallery) {
						$portfo_img = aq_resize($gallery['url'],263,210,true,true,true); 
						echo '<div class="col-md-3 col-lg-3 col-xs-12 col-sm-6 marginbottom20"><div class="text-center animated fadeInUp delay-2500 smalleff effect6"><a href="'. $gallery['url'] .'" rel="prettyPhoto" ><img src="'. $portfo_img .'" alt="Comet Decoration & Design '. $post->post_title . '" /></a></div></div>';
					} ?>
				</div>
			<?php endif ?>
		</div>
		<!-- <hr> -->
		<div class="row">
			<div id="location">
				<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 animated fadeInDownShort delay-3500">
					<h4 class="fontlora text-center marginbottom30 f30"><span class="dot"></span>HOTEL LOCATION<span class="dot1"></span></h4>
				</div>
				<div class="col-xs-12 col-sm-8 col-lg-8 col-md-8">
					<?php $lat = $hoteldata['location']['0']['lat'];$lang = $hoteldata['location']['0']['lang']; ?>
					<div class="margintop20 marginbottom50  animated growIn delay-3500">
						<a class="office_map_link btnhotel" href="#office_map"  rel="<?php echo $lat; ?>,<?php echo $lang; ?>" style="display:none;"></a>
						<div class="office_map_link" id="office_map" style="width:100%;height:400px;"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-4 col-md-4 animated growIn delay-3500">
					<?php if ($hoteldata['hotel_address']): ?>
                        <div class="addwrap margintop20">
                            <h3 class="fontlora">Address</h3>
                            <i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $hoteldata['hotel_address']; ?>
                        </div>
                    <?php endif ?>
					<?php if ($hoteldata['hotel_phone']|$hoteldata['hotel_email']): ?>
                        <div class="addwrap margintop20">
                            <h3 class="fontlora">Contact Us</h3>
                            <?php if ($hoteldata['hotel_phone']): ?>
                            <div><i class="fa fa-mobile" aria-hidden="true"></i><?php echo $hoteldata['hotel_phone']; ?></div>
                            <?php endif ?>
                            <?php if ($hoteldata['hotel_email']): ?>
                            <div><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo $hoteldata['hotel_email']; ?></div>
                            <?php endif ?>
                        </div>
                    <?php endif ?>
				</div>
			</div>
		</div>
	</div>
</div>
	

<script type="text/javascript">
$(document).ready(function(){
  $('a[href^="#"]').on('click',function (e) {
      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
          'scrollTop': $target.offset().top
      }, 1200, 'swing', function () {
          window.location.hash = target;
      });
  });
});

    function getLatLng(str) {
        var arr = str.split(',');
        var lat = jQuery.trim(arr[0]);
        var lng = jQuery.trim(arr[1]);
        return new Array(lat,lng);
    }

    function office_map_initialize() {  
        var officeLatLngStr = jQuery('a.office_map_link:eq(0)').attr('rel');    
        var officeLatLngArr = getLatLng(officeLatLngStr);   
        var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
        var map = new google.maps.Map(document.getElementById("office_map"), {
            zoom: 12,
            center: officeLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        jQuery('a.office_map_link').each(function() {
        	
            var office = jQuery(this);
            var officeLatLngStr = office.attr('rel');
            var officeLatLngArr = getLatLng(officeLatLngStr);
            var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
            var marker = new google.maps.Marker({
                clickable: false,
                flat: false,
                position: officeLatLng,
                map: map

            });
            office.bind('click', function(e) {
                var office = jQuery(this);
                var officeLatLngStr = office.attr('rel');
                var officeLatLngArr = getLatLng(officeLatLngStr);
                var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
                map.panTo(officeLatLng);
            });
        }); 

        
    }
    office_map_initialize();
    
    </script>
<?php get_footer() ?>