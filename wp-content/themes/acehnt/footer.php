</div> <!-- end of content -->

<?php global $THEME_OPTIONS; ?>
<footer>
	<div class="ftop">
		<div class="container">
			<div class="fbox col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<?php 
				$about = get_post( ACE_PAGE_ABOUT );  
				if(strlen($about->post_excerpt)>150){
					$excerpt = substr($about->post_excerpt, 0, 150)."..."; 	
				}else{
					$excerpt = $about->post_excerpt;
				}
			?>
				<?php if ($excerpt): ?>
					<?php echo apply_filters("the_content", $excerpt); ?>
				<?php endif ?>
				<?php if ($THEME_OPTIONS['info_hotline']): ?>
					<p><i class="fa fa-mobile" style="margin-right:6px;" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_hotline']; ?></p>
				<?php endif ?>
				<?php if ($THEME_OPTIONS['info_website']): ?>
					<p><i class="fa fa-globe" style="margin-right:6px;" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_website']; ?></p>
				<?php endif ?>
			</div>
			<div class="fbox col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<?php
	                    wp_nav_menu(array(
	                        'theme_location' => 'sitemap',
	                        'menu_id' => 'fmenu',
                        ));
                	?>
			</div>
			<div class="fbox col-xs-12 col-sm-4 col-md-4 col-lg-4 fright">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Facehotelsandtourismgroup&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>					
		</div>
	</div>
	<div class="fbottom">
		<div class="container">
			<div class="copyright col-xs-12 col-sm-7 col-md-9 col-lg-9">
				Copyrights <?php echo date('Y'); ?> &COPY;
			</div>
			<div class="developby col-xs-12 col-sm-5 col-md-3 col-lg-3 fright">
				Developed by <a href="http://www.digitaldots.com.mm" target="_blank">Digital Dots</a></div>
			</div>					
		</div>
	</div>			
</footer>
<?php wp_footer(); ?>
</body>
</html>