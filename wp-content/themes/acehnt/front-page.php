<?php get_header(); ?>

<?php 
	if ( function_exists( 'soliloquy' ) ) { soliloquy( '24' ); }
?>
<div class="row wc-home">
	<div class="container">
		<div class="wc-home-1 col-xs-12 col-sm-7 col-md-7 col-lg-7">
		<?php $about = get_post( ACE_PAGE_ABOUT );  ?>
		<?php $thumbail = wp_get_attachment_image_src( get_post_thumbnail_id($about->ID), 'full' ); ?>

			<div class="wc-home-left">
				<h5>WELCOME TO <span class="line"></span></h5>
				<h1 class="title-1">ACE</h1> <h1 class="title-2">Hotels & Tourism</h1>
				<div class="margintop20"><?php echo apply_filters("the_content", $about->post_excerpt); ?></div>
				<a class="readmore" href="<?php echo get_permalink($about->ID); ?>">Read more</a>
			</div>
		</div>
		<div class="wc-home-2 col-xs-12 col-sm-5 col-md-5 col-lg-5">
			<div class="wc-home-right">
				<img src="<?php echo $thumbail['0']; ?>" alt="pic1"/>
			</div>
		</div> 
	</div>     
</div><!-- end Welcome -->

<?php
    $args = array(
        'post_type' => ACE_TYPE_HOTEL,
        'posts_per_page' => -1,
        'post_status' => 'publish',
        );
    $hotels = get_posts($args);

    $first_hotel = $hotels[0];
    $hotel_link = get_permalink($first_hotel->ID);
    $galleries = get_field('gallery',$first_hotel->ID);
    $rooms = get_field('room',$first_hotel->ID);
    $faciliteis = get_field('front_facilities',$first_hotel->ID);

    $second_hotel = $hotels[1];
    $sec_hotel_link = get_permalink($second_hotel->ID);
    $sec_galleries = get_field('gallery',$second_hotel->ID);

    $other_hotels = array_slice($hotels,2);
?>
<!-- center slide -->
<div class="row">
	<div class="col-xs-12 nopadding">
		<div class="content-1">
			<div class="container">
				<div class="row">
					<h2 class="fontlora"><?php echo $first_hotel->post_title; ?></h2>
					<p><?php echo $first_hotel->post_excerpt; ?></p>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-2 col-lg-2"></div>   
					<div class="col-xs-10 col-sm-10 col-md-8 col-lg-8">
						<div class="slider-content-1">
							<div data-ride="carousel" class="carousel slide" id="carousel1">                     
								<!-- Wrapper for slides -->
								<div role="listbox" class="carousel-inner">
								<?php $y=1; foreach ($galleries as $gallery): ?>
									<?php $hotel_image = aq_resize($gallery['url'], 730, 480, true, true, true); ?>
									<div class="item <?php if ($y == 1) { echo "active"; } ?> hover ehover1">
										<a href="<?php echo $hotel_link; ?>"><img alt="img-1" src="<?php echo $hotel_image; ?>"></a>
										<div class="carousel-caption">
											<h4><?php echo $gallery['title']; ?></h4>
										</div>
									</div>
								<?php $y++;endforeach ?>
								</div>

								<a data-slide="prev" role="button" href="#carousel1" class="left carousel-control">
									<span aria-hidden="true" class="glyphicon glyphicon-menu-left"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a data-slide="next" role="button" href="#carousel1" class="right carousel-control">
									<span aria-hidden="true" class="glyphicon glyphicon-menu-right"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-2 col-lg-2"></div>
				</div>
			</div>
		</div>              
	</div>
</div>
<!-- End center slide -->

<!-- ROOM TYPE -->
<div class="row content-2"> 
	<div class="container">
		<div class="col-xs-12">
			<div class="row">
				<div class="content-2-left col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<div class="title-content col-xs-12">
							<h2 class="fontlora">ROOM TYPE</h2> 
							<span class="dot"></span>
						</div>
						<?php foreach ($rooms as $room):
							$image = aq_resize( $room['room_image']['url'] , 233, 155, true, true, true);  ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<a href="#">
								<img src="<?php echo $image; ?>" alt="<?php echo $room['room_title']; ?>"/> 
								<h5 class="text-uppercase"><?php echo $room['room_title']; ?></h5>
							</a>
						</div>
						<?php endforeach ?>
					</div>
				</div>
				<div class="content-2-right col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="row"> 
						<div class="title-content col-xs-12">
							<h2 class="fontlora"> FACILITIES </h2>
							<span class="dot"></span>
						</div>
						<?php foreach ($faciliteis as $facility):
						$image = aq_resize( $facility['image']['url'] , 233, 90, true, true, true);  ?>
						<div class="img-right col-xs-12 col-sm-6 col-md-6 col-lg-6"> 
							<div class="img-1">
								<a href="#">
									<img src="<?php echo $image; ?>" alt="<?php echo $facility['title']; ?>"/>
									<h5><?php echo $facility['title']; ?></h5>
								</a>
							</div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
			<div class="read-more row">
				<a class="readmore" href="<?php echo $hotel_link; ?>">Read more</a>
			</div>
		</div>
	</div>
</div>
<!-- End ROOM TYPE -->

<!-- left slider -->
<div class="row content-3">
    <div class="container">
        <div class="col-xs-12">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <div data-ride="carousel" class="carousel slide" id="carousel2">
                    <!-- Wrapper for slides -->
                    <div role="listbox" class="carousel-inner">
                        <?php $y=1; foreach ($sec_galleries as $sec_gallery): ?>
                        <?php $sec_hotel_image = aq_resize($sec_gallery['url'], 730, 480, true, true, true); ?>
                        <div class="item <?php if ($y == 1) { echo "active"; } ?> hover ehover1">
                            <a href="<?php echo $sec_hotel_link; ?>"><img alt="img-1" src="<?php echo $sec_hotel_image; ?>"></a>
                            <div class="carousel-caption">
                                <h4><?php echo $sec_gallery['title']; ?></h4>
                            </div>
                        </div>
                        <?php $y++;endforeach ?>
                    </div>
                    <a data-slide="prev" role="button" href="#carousel2" class="left carousel-control">
                        <span aria-hidden="true" class="glyphicon glyphicon-menu-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a data-slide="next" role="button" href="#carousel2" class="right carousel-control">
                        <span aria-hidden="true" class="glyphicon glyphicon-menu-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <h2 class="title fontlora"><?php echo $second_hotel->post_title; ?></h2>
                <p><?php echo $second_hotel->post_excerpt; ?></p>
                <a class="readmore" href="<?php echo $sec_hotel_link; ?>">Read more</a>
            </div>
        </div>
    </div>
</div>
<!-- End left slider -->

<!-- content-4 -->
<div class="row content-4">
	<div class="container">
		<div class="col-xs-12 ">
			<div class="read-more">
				<h2 class="fontlora"><span class="dot"></span>Other Hotels<span class="dot1"></span></h2>
			</div>
		</div>
		<div class="col-xs-12 listhotels">	
			<div class="bxSlider">				
				<div class="slider-4">
				<?php foreach ($other_hotels as $other_hotel) { ?>
				<?php $oth_hotel_link = get_permalink($other_hotel->ID); ?>
				<?php $oth_img_url = wp_get_attachment_image_src(get_post_thumbnail_id($other_hotel->ID), 'full'); ?>
				<?php $oth_hotel_image = aq_resize($oth_img_url[0], 355, 240, true, true, true); ?>
					<div class="slide hover ehover5">

						<a href="#"><img src="<?php echo $oth_hotel_image; ?>"></a>
						<div class="bxSlider-caption">
							<h3><a href="<?php echo $oth_hotel_link; ?>"><?php echo $other_hotel->post_title; ?></a></h3>
							<h5><a href="<?php echo $oth_hotel_link; ?>">readmore</a></h5>
						</div>
					</div>					
				<?php } ?>
				</div>	
			</div>	
		</div>
		<div class="col-xs-12 ">
			<div class="read-more row">
				<a class="readmore" href="<?php echo ACE_TYPE_HOTEL; ?>">View all hotels</a>
			</div>
		</div>
	</div>
</div>
<!-- End content-4 -->


<!-- Content 5 -->
<div class="row content-5">
	<div class="container">
		<div class="col-xs-12">

			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 content-5-left">
				<?php $travels = get_post( ACE_PAGE_TRAVELS );  ?>
				<?php $travel_thumbail = wp_get_attachment_image_src( get_post_thumbnail_id($travels->ID), 'full' ); ?>
				<h2 class="title-1 fontlora">ACE</h2>
				<h2 class="title-2 fontlora">Travels</h2>
				<div class="borderbot"><?php echo apply_filters("the_content", $travels->post_excerpt); ?></div>
				<?php 
					$infotravel = get_field('ace_travel_info',$travels->ID); 
					$infocount = 1;
					foreach ($infotravel as $infotra) {								
						if($infocount == 1 || $infocount%2==1){
							echo "<div class=\"row buttons\">";
						}
				?>
					<div class="button-1 col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="img-left"><img src="<?php echo $infotra['info_icon']['url']; ?>" alt="<?php echo $infotra['info_name']; ?>"/></div>
						<a class="readmore1" target="__blank" href="<?php echo $infotra['info_link']; ?>"><?php echo $infotra['info_name']; ?></a>
					</div>
					<?php if ($infocount%2 == 0): ?>
						<?php echo "</div>"; ?>
					<?php endif ?>
				<?php $infocount++;} ?>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 content-5-right">
				<img src="<?php echo $travel_thumbail[0]; ?>" alt="<?php echo $travels->post_title; ?>">
				<div class="des">
					<a class="readmore2" href="<?php echo get_permalink($travels->ID); ?>" target="__blank">Read more</a>
					<a class="readmore2 clicktour" href="http://www.acetravelsandtours.com/tour-packages-page/" target="__blank">Click Our Tours</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Content 5 -->

<!-- Latest news -->
<div class="row latestnews">
<?php     
	$args = array(
		'posts_per_page' => 4,
		'post_status' => 'publish',
		'category_name' => 'news',
	);
	$news = get_posts($args);
?>	
	<div class="container">
		<div class="col-xs-12 ">
			<div class="read-more">
				<h2 class="fontlora"><span class="dot"></span>Latest news<span class="dot1"></span></h2>
			</div>
		</div>
	</div>
	<div class="container latestnews-content">
		<div class="row">
		<?php 
			$count=1; 
			foreach ($news as $new):
			$new_thumbail = wp_get_attachment_image_src( get_post_thumbnail_id($new->ID), 'full' );
			$new_image = aq_resize($new_thumbail[0], 335, 225, true, true, true);
			if($count == 1 || $count%2==1){
				echo "<div class=\"row\">";
			}
		?>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="latest-news-1 col-xs-5 col-sm-5 col-md-5 col-lg-5">
					<a href="#" class="hover ehover11">
						<img src="<?php echo $new_image; ?>" alt="img">
					</a>
				</div>
				<div class="latest-news-2  col-xs-7 col-sm-7 col-md-7 col-lg-7">
					<div class="date"><?php the_time('M j, Y'); ?><span class="line"></span></div>
					<div class="title"><a href="#"><?php echo $new->post_title; ?></a></div>
					<div class="description"><?php echo $new->post_excerpt; ?></div>
					<a class="readmore3" href="<?php echo get_permalink($new->ID); ?>">Read more</a>
					<hr>
				</div>
			</div>		
			<?php if ($count%2 == 0): ?>
						<?php echo "</div>"; ?>
			<?php endif ?>
			<?php $count++; ?>	
		<?php endforeach ?>
		</div>
	</div>
	<div class="col-xs-12 ">
		<div class="read-more row">
			<a class="readmore" href="/category/news/">View all</a>
		</div>
	</div>
</div>
<!-- End Latest news -->

<?php get_footer(); ?>