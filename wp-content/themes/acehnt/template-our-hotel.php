<?php /* Template Name: Our Hotel */ ?>
<?php get_header() ?>

<div id="banner">
	<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	<div class="container text-center">
		<h1 class="bannerheader pagetitle text-uppercase fontlora"><?php the_title() ?></h1>
	</div>
</div>

<div id="ourhotels">
	<div class="container">
		<div class="ourhotelswrapper">
		<div class="row">
		    <div class="col-md-6 marginbottom10">
		    					<div class="event-classic-item">
				<div class="inner-post">
					<a href="single-room-gallery.html" title="King Suite Room">
					<img src="<?php echo ASSET_URL; ?>images/villa2.jpg" class="attachment-thumb_370x370 wp-post-image" alt="King Suite Room"></a>                            
					<div class="light-box">
					</div>
					<div class="info-classic">
						<div class="product-price">
							<span class="price"><span class="amount">$&nbsp;2,000</span></span>
						</div>
						<div class="title">
							<a href="single-room-gallery.html" title="King Suite Room">The Hotel Royal ACE</a>
						</div>
						<div class="classic-read-more">
							<a class="btnhotel" href="single-room-gallery.html" title="King Suite Room">Detail <i class="fa fa-angle-right"></i></a>
							<a class="office_map_link btnhotel" href="#office_map" rel="21.135712,94.863330">See Map</a>
						</div>
					</div>
				</div>
				</div>
		    </div>
		    <div class="col-md-6 marginbottom10">
				<div class="event-classic-item">
				<div class="inner-post">
					<a href="single-room-gallery.html" title="King Suite Room">
					<img src="<?php echo ASSET_URL; ?>images/villa2.jpg" class="attachment-thumb_370x370 wp-post-image" alt="King Suite Room"></a>                            
					<div class="light-box">
					</div>
					<div class="info-classic">
						<div class="product-price">
							<span class="price"><span class="amount">$&nbsp;2,000</span></span>
						</div>
						<div class="title">
							<a href="single-room-gallery.html" title="King Suite Room">The Hotel Royal ACE</a>
						</div>
						<div class="classic-read-more">
							<a class="btnhotel" href="single-room-gallery.html" title="King Suite Room">Detail <i class="fa fa-angle-right"></i></a>
							<a class="office_map_link btnhotel" href="#office_map" rel="21.135712,94.863330">See Map</a>
						</div>
					</div>
				</div>
				</div>

		    </div>
		</div>


    	<div class="row">
    		<div class="col-lg-3 col-md-3 col-sm-3">
            	<div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa3.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>BawgaTheiddihi Hotel (Bagan)</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>BawgaTheiddihi Hotel (Bagan)</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
						                <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
						                <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="21.135712,94.863330">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
            	</div>
        	</div>
    		<div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa4.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>BawgaTheiddihi Hotel (Kyaikhteeyoe)</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>BawgaTheiddihi Hotel (Kyaikhteeyoe)</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="17.481184,97.098857">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
        	</div>
    		<div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa5.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>Hotel ACE Chaung Thar</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>Hotel ACE Chaung Thar</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="16.970756,94.451462">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
	        </div>
    
    		<div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa3.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>Hotel ACE Ngwe Saung</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>Hotel ACE Ngwe Saung</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel=" ">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
	        </div>
        </div> <!-- end of row -->

        <div class="row">
	        <div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa3.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>Hotel ACE Naypyitaw</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>Hotel ACE Naypyitaw</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="16.821486,94.397329">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa5.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>Hotel ACE Ngapali</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>Hotel ACE Ngapali</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="21.135712,94.863330">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa4.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>BawgaTheiddihi Hotel (Ngapali)</h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>BawgaTheiddihi Hotel (Ngapali)</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="21.135712,94.863330">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-3">
	            <div class="single_room_wrapper animatedParent clearfix">
	                <figure class="uk-overlay uk-overlay-hover">
	                    <div class="room_media">
	                        <a href="#"><img src="<?php echo ASSET_URL; ?>images/villa3.jpg" alt=""></a>
	                    </div>
	                    <div class="room_title border-bottom-whitesmoke clearfix">
	                        <div class="left_room_title floatleft">
	                            <h6>Serenity Garden Resort </h6>
	                        </div>
	                    </div>
	                    <div class="uk-overlay-panel uk-overlay-background single_wrapper_details clearfix animated fadeInLeft">
	                        <div class="border-dark-1 padding-22 clearfix single_wrapper_details_pad">
	                            <h5>Serenity Garden Resort</h5>
	                            <div class="single_room_cost clearfix">
	                                <div class="floatleft">
	                  <p>No 10 , Myat lay Street , Anaw Ra Htar Block ,Na Ra Theinga Quarter, Bagan, Myanmar</p>
	                  <p>061-65427</p>
	                                    <a class="btnhotel" href="#" >Detail</a>
	                                    <a class="office_map_link btnhotel" href="#office_map" rel="21.135712,94.863330">See Map</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </figure>
	            </div>
	        </div>
    	</div> <!-- end of row -->
		<div class="row">
			<div class="col-md-12">
				<h2 class="fontlora text-center"><span class="dot"></span>HOTEL LOCATION<span class="dot1"></span></h2>
				<div class="margintop20 marginbottom50">
					<div id="office_map" style="width:100%;height:400px;"></div>
				</div>
			</div>
		</div>
	</div>
	</div> <!-- end of container -->
</div>

<script>
  // $(document).ready(function(){

  //  var mycenter = new google.maps.LatLng(16.785029,96.155150);
  //  function initMap() {
  //    map = new google.maps.Map(document.getElementById('googleMap'), {
  //      center: mycenter,
  //      zoom: 17,
  //      mapTypeId:google.maps.MapTypeId.ROADMAP
  //    });

  //    marker=new google.maps.Marker({
  //             position: mycenter,
  //      map: map,
  //      animation: google.maps.Animation.DROP
  //    });
      
  //    var contentString = '<div id="infowindow">'+' <div class="text-center marginbottom20"><img src="<?php echo $THEME_OPTIONS['logo'] ?>" alt="Comet Design & Decoration Logo" /></div>'+'<div id="bodyinfowindow">'+
  //    '<?php if($THEME_OPTIONS['info_address']): ?><p><i class="fa fa-map-marker"></i> : <?php echo $THEME_OPTIONS['info_address'];?></p><?php endif; ?>'+
  //    '<?php if($THEME_OPTIONS['info_hotline']): ?><p><i class="fa fa-phone"></i> : <?php echo $THEME_OPTIONS['info_hotline'];?></p><?php endif; ?>'+
  //    '<?php if($THEME_OPTIONS['info_email']): ?><p><i class="fa fa-envelope"></i> : <?php echo $THEME_OPTIONS['info_email'];?></p><?php endif; ?>'+
  //    '<?php if($THEME_OPTIONS['info_website']): ?><p><i class="fa fa-globe"></i> : <?php echo $THEME_OPTIONS['info_website'];?></p><?php endif; ?>'+
  //    '</div>'+'</div>';

  //    var infowindow = new google.maps.InfoWindow({
 //             content: contentString
 //         });

  //    marker.setMap(map);
  //    marker.addListener('click', function() {
 //             infowindow.open(map, marker);
 //         });
      
  //  }

 //     google.maps.event.addDomListener(window, 'load', initMap);
 //    });


</script>

<script type="text/javascript">
$(document).ready(function(){
  $('a[href^="#"]').on('click',function (e) {
      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
          'scrollTop': $target.offset().top
      }, 1200, 'swing', function () {
          window.location.hash = target;
      });
  });
});

    function getLatLng(str) {
        var arr = str.split(',');
        var lat = jQuery.trim(arr[0]);
        var lng = jQuery.trim(arr[1]);
        return new Array(lat,lng);
    }

    function office_map_initialize() {  
        var officeLatLngStr = jQuery('a.office_map_link:eq(0)').attr('rel');    
        var officeLatLngArr = getLatLng(officeLatLngStr);   
        var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
        var map = new google.maps.Map(document.getElementById("office_map"), {
            zoom: 12,
            center: officeLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        jQuery('a.office_map_link').each(function() {
            var office = jQuery(this);
            var officeLatLngStr = office.attr('rel');
            var officeLatLngArr = getLatLng(officeLatLngStr);
            var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
            var marker = new google.maps.Marker({
                clickable: false,
                flat: false,
                position: officeLatLng,
                map: map

            });
            office.bind('click', function(e) {
                var office = jQuery(this);
                var officeLatLngStr = office.attr('rel');
                var officeLatLngArr = getLatLng(officeLatLngStr);
                var officeLatLng = new google.maps.LatLng(officeLatLngArr[0], officeLatLngArr[1]);
                map.panTo(officeLatLng);
            });
        }); 

        
    }
    office_map_initialize();
    
    </script>

<?php get_footer() ?>