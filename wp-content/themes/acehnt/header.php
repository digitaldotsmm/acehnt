<?php global $THEME_OPTIONS; ?>
<!doctype html>
<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title><?php wp_title(''); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<?php //var_dump(TEMPLATEPATH); ?>
	<?php if ( file_exists(TEMPLATEPATH .'/favicon.jpg') ) : ?>
		<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.jpg">
	<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>

</head>
<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() )echo $body_classes; ?>">
	<div class="se-pre-con"></div>
	
<div class="header-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="logo">
					<a href="<?php echo WP_HOME; ?>">
						<img src="<?php echo $THEME_OPTIONS['logo'] ?>" alt="Ace Hotels & Tourism" />                
					</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class="header-nav">
					<div class="container-fluid nopadding">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
					<?php
	                    wp_nav_menu(array(
	                        'theme_location' => 'main',
	                        'menu_class' => 'nav nav-pills',
	                        'menu_id' => 'main-menu',
	                        'container_class' => 'collapse navbar-collapse',
	                        'container_id' => 'myNavbar',
	                        'after' => '<span class="line"></span>',
                        ));
                	?>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="content">    