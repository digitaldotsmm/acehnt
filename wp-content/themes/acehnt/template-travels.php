<?php /* Template Name: Travels */ ?>

<?php get_header(); global $THEME_OPTIONS; ?>

	<div id="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
		<div class="container text-center">
			<h1 class="bannerheader pagetitle text-uppercase fontlora"><?php the_title() ?></h1>
		</div>
	</div>
<section class="content-5">
	
	<div class="container ">
		<div class="row  margintop50 marginbottom50">
			<div class="col-md-8">
				<div class="pagecontent">
					<?php echo apply_filters("the_content",$post->post_content); ?>
				</div>
			</div>
			<div class="col-md-4">
				<?php $img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
				<div class="aboutimg text-center">
					<img src="<?php echo $img_url[0]; ?>">
				</div>
			</div>
		</div>
	</div>
</section>
	<div>
	<section class="content-4">
		<div class="container">
			<div class="row">
				<?php 
					$infotravel = get_field('ace_travel_info',$travels->ID); 
					foreach ($infotravel as $infotra) {								
						
				?>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="sonareffect marginbottom20 text-center">
						<div class="icon hi-icon-archive">
							<a href="<?php echo $infotra['info_link']; ?>" target="__blank"><img src="<?php echo $infotra['info_icon']['url']; ?>" alt="<?php echo $infotra['info_name']; ?>"></a>
						</div>
						<a href="<?php echo $infotra['info_link']; ?>" target="__blank"><div class="travelinfosg"><?php echo $infotra['info_name']; ?></div></a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row margintop50 marginbottom30">
			<div class="col-md-2"></div>
				<?php $travel_info = get_field('travel_info',$post->ID); ?>
				
				<?php foreach ($travel_info as $travel): ?>
					<div class="col-md-4 text-center">
					<h3 class="fontlora greencolor marginbottom10 traveltitle">Branch Number</h3>
					<?php echo apply_filters('the_content',$travel['contact_travel']); ?>					
					</div>
				<?php endforeach ?>
				
			<div class="col-md-2"></div>
			</div>	
		</div>
	</section>
		
	</div>
<?php get_footer(); ?>
